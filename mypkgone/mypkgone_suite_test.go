package mypkgone_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestMyPkgOne(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "MyPkgOne Suite")
}
