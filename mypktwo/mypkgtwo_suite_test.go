package mypkgtwo_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestMyPkgTwo(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "MyPkgTwo Suite")
}
